/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.net;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AtProtocol extends AbstractProtocol {
	private static final long serialVersionUID = 2962839408786538103L;


	/**
	 * This pattern is used to extract the identifier ("@[" and "]").<br/>
	 * <code>$1</code> contains the message text without "@[" and "]".
	 */
	private final String IDENTIFIER_PATTERN = "@\\[(.+)\\]";
	
	
	/**
	 * This pattern is used to match the key.<br/>
	 * <br/>
	 * <b>Example:</b><br/>
	 * <code>
	 * 	<b>stringKey</b>:"This is a quoted value.."<br/>
	 * 	<b>anotherKey</b>:12.0
	 * </code>
	 */
	private final String KEY_PATTERN = "([\\w]+:)";
	
	
	/**
	 * This pattern is used to match a quoted value.<br/>
	 * <br/>
	 * <b>Example:</b><br/>
	 * <code>
	 * 	stringKey:<b>"This is a quoted value.."</b><br/>
	 * 	anotherKey:12.0
	 * </code>
	 */
	private final String QUOTED_VALUE_PATTERN = "\"[\\w\\s\\d\\+/!\\.]+\"";
	
	
	/**
	 * This pattern is used to match an unquoted value.<br/>
	 * <br/>
	 * <b>Example:</b><br/>
	 * <code>
	 * 	stringKey:"This is a quoted value.."<br/>
	 * 	anotherKey:<b>12.0</b>
	 * </code>
	 */
	private final String UNQUOTED_VALUE_PATTERN = "[\\w\\d\\+/!\\.]+";
	
	
	/**
	 * This pattern will match any of the following phrases:
	 * <ul>
	 * 	<li>test:"This is a test"</li>
	 * 	<li>number:123</li>
	 * 	<li>string:line</li>
	 * </ul>
	 * 
	 * @see #KEY_PATTERN
	 * @see #QUOTED_VALUE_PATTERN
	 * @see #UNQUOTED_VALUE_PATTERN
	 */
	private final String PATTERN = KEY_PATTERN+"("+QUOTED_VALUE_PATTERN+"|"+UNQUOTED_VALUE_PATTERN+")";
	
	
	/**
	 * This pattern is compiled
	 * 
	 * @see #PATTERN
	 */
	private final Pattern COMPILED_PATTERN = Pattern.compile(PATTERN);
	
	@Override
	public Message parseText(String rawText) {
		Message message = newMessage();
		rawText = stripIdentifier(rawText);
		Matcher matcher = COMPILED_PATTERN.matcher(rawText);
		while (matcher.find()) {
			String group = matcher.group();
			String[] pair = group.split(":");
			String key = pair[0];
			String value = pair[1];
			if (value.charAt(0) == '"' && value.charAt(value.length() - 1) == '"') {
				value = value.substring(1, value.length() - 1);
			}
			message.put(key, value);
		}
		return message;
	}

	/**
	 * Removes "@[" and "]" from the string.
	 * 
	 * @param text
	 * @return
	 */
	private String stripIdentifier(String text) {
		return text.replaceAll(IDENTIFIER_PATTERN, "$1");
	}
	
	@Override
	public String toText(Message message) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("@[");
		Set<String> keys = message.keySet();
		for (String key : keys) {
			Object value = message.getObject(key);
			if (shouldBeQuotedString(value)) {
//				System.out.println(value+" should be quoted");
				value = String.format("\"%s\"", value);
//				System.out.println("Quoted: "+value);
			}
			stringBuilder.append(String.format("%s:%s,", key, value));
		}
		stringBuilder.setLength(stringBuilder.length() - 1);
		stringBuilder.append("]");
		return stringBuilder.toString();
	}
	
	private boolean shouldBeQuotedString(Object obj) {
		boolean shouldBeQuotedString;
		try {
			shouldBeQuotedString = ((String)obj).contains(" ");
		} catch (ClassCastException e) {
			/* This exception is only caught if value is not a String. */
			shouldBeQuotedString = false;
		}
		return shouldBeQuotedString;
	}
	
//	public static void main(String[] args) {
//		Protocol protocol = new AtProtocol();
//		Message message = protocol.parseText("@[message:\"hej med dig\"]");
//		System.out.println(message.getString("message"));
//	}
}
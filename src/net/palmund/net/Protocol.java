/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.net;

import java.io.Serializable;

/**
 * This class represents a protocol which handles two important areas in
 * relation to server transmissions.
 * <ol>
 * <li>It parses raw text transmissions from a server translating them into
 * {@link Message} objects.</li>
 * <li>It translates {@link Message} objects into raw text that can be
 * transmitted to a server or client.</li>
 * </ol>
 * 
 * @author S�ren Palmund (palmund@gmail.com)
 * @version 1.1
 */
public interface Protocol extends Serializable {
	/**
	 * Creates a {@link Message} from a chunk of raw text.
	 * 
	 * @param text
	 *            the text received to be transformed into a {@link Message}
	 * @return a {@link Message} object that contains the key/value pairs
	 *         defined by
	 */
	Message parseText(String text);

	/**
	 * Formats the message according to the protocol. Subclasses should use this
	 * to ensure that the data they transmit conforms to the protocol on the
	 * other end.
	 * 
	 * @param message
	 *            a {@link Message}
	 * @return the contents of the message in raw text
	 */
	String toText(Message message);

	/**
	 * Creates an empty {@link Message}.
	 * 
	 * @return a {@link Message}
	 */
	Message newMessage();

	/**
	 * Creates a {@link Message} with the same key/value pairs as
	 * <code>message</code>.
	 * 
	 * @param message
	 *            a {@link Message}
	 * @return a {@link Message}
	 */
	Message extendMessage(Message message);
}
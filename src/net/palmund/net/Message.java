/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.net;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class is the basic most object that the {@link Protocol} will return
 * upon calling {@link Protocol#parseText(String)}. The message object itself
 * does not know how to represent itself as text. A {@link Protocol} is needed
 * for such transformation.
 * 
 * @author S�ren Palmund (palmund@gmail.com)
 * @version 1.1
 */
public class Message implements Serializable {
	private static final long serialVersionUID = -301041218901832343L;
	
	private Protocol protocol;
	
	protected Map<String, Object> parameters;

	protected Message(Protocol protocol) {
		this(protocol, new HashMap<String, Object>());
	}

	protected Message(Protocol protocol, Message message) {
		this(protocol, message.parameters);
	}

	protected Message(Protocol protocol, Map<String, Object> parameters) {
		this.parameters = parameters;
		this.protocol = protocol;
	}

	public boolean getBoolean(String key) {
		return Boolean.parseBoolean(getString(key));
	}

	public long getLong(String key) {
		return Long.parseLong(getString(key));
	}

	public int getInt(String key) {
		return Integer.parseInt(getString(key));
	}

	public double getDouble(String key) {
		return Double.parseDouble(getString(key));
	}

	public float getFloat(String key) {
		return Float.parseFloat(getString(key));
	}

	public String getString(String key) {
		return String.valueOf(getObject(key));
	}

	public Object getObject(String key) {
		return parameters.get(key);
	}

	public Set<String> keySet() {
		return parameters.keySet();
	}

	/**
	 * Returns whether the message contains the specific <code>key</code>.
	 * 
	 * @param key
	 * @return <code>true</code> if the key exists, <code>false</code> otherwise
	 */
	public boolean hasKey(String key) {
		return parameters.containsKey(key);
	}

	public void put(String key, Boolean value) {
		parameters.put(key, value ? Boolean.TRUE : Boolean.FALSE);
	}

	public void put(String key, Collection<?> value) {
		parameters.put(key, (Object) value);
	}

	public void put(String key, Map<?, ?> value) {
		parameters.put(key, (Object) value);
	}

	public void put(String key, Integer value) {
		parameters.put(key, value);
	}

	public void put(String key, Float value) {
		parameters.put(key, value);
	}

	public void put(String key, Double value) {
		parameters.put(key, value);
	}

	public void put(String key, String value) {
		parameters.put(key, value);
	}

	public void put(String key, Object value) {
		parameters.put(key, value);
	}

	@Override
	public String toString() {
		return protocol.toText(this);
	}
}
/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.net;

public abstract class AbstractProtocol implements Protocol {
	private static final long serialVersionUID = 4727905368163280270L;

	@Override
	public Message newMessage() {
		return new Message(this);
	}

	@Override
	public Message extendMessage(Message message) {
		return new Message(this, message);
	}
}
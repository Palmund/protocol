/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.net;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This protocol provides the implementation necessary to send and receive {@link Message}s in the
 * JSON format.
 * 
 * @author S�ren Palmund (palmund@gmail.com)
 * @version 1.0
 */
public class JSONProtocol extends AbstractProtocol {
	private static final long serialVersionUID = 6962134666514534837L;

	@Override
	public Message parseText(String rawText) {
//		MessageBuilder message = newMessage();
		Message message = new Message(this);
//		Map<String, Object> parameterMap = new HashMap<String, Object>();
		try {
			JSONObject parsedSource = new JSONObject(rawText);
			for (Object key : parsedSource.keySet()) {
				String parsedKey = (String) key;
				Object parsedValue = parsedSource.get(parsedKey);
				Object interpretedValue = interpret(parsedValue);
				message.put(parsedKey, interpretedValue);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return message; //.toMessage();
	}

	private Object interpret(Object value) throws JSONException {
		if (value instanceof JSONArray) {
			return handleCollection((JSONArray) value);
		} else if (value instanceof JSONObject) {
			return handleMap((JSONObject) value);
		} else {
			return value;
		}
	}

	private List<Object> handleCollection(JSONArray collection) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < ((JSONArray) collection).length(); i++) {
			Object value = ((JSONArray) collection).get(i);
			list.add(value);
		}
		return list;
	}

	private Map<Object, Object> handleMap(JSONObject jsonMap) throws JSONException {
		Map<Object, Object> map = new HashMap<Object, Object>();
		JSONArray names = ((JSONObject) jsonMap).names();
		for (int i = 0; i < names.length(); i++) {
			Object key = names.get(i);
			Object value = ((JSONObject) jsonMap).get((String) key);
			map.put(key, value);
		}
		return map;
	}

	@Override
	public String toText(Message message) {
		JSONObject json = new JSONObject();
		Set<String> keys = message.keySet();
		for (String key : keys) {
			Object value = message.getObject(key);
			handle(json, key, value);
		}
		return json.toString();
	}

	private void handle(JSONObject json, String key, Object value) {
		try {
//			if (value instanceof Boolean) {
//				json.put(key, (Boolean) value);
//			} else if (value instanceof Double) {
//				json.put(key, (Double) value);
//			} else 
			if (value instanceof Collection<?>) {
				json.put(key, (Collection<?>) value);
			} else {
				json.put(key, value);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
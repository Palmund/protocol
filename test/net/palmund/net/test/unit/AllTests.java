package net.palmund.net.test.unit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AtProtocolTest.class, JSONProtocolTest.class })
public class AllTests {}
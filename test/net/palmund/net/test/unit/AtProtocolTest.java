package net.palmund.net.test.unit;

import static org.junit.Assert.*;

import net.palmund.net.AtProtocol;
import net.palmund.net.Message;
import net.palmund.net.Protocol;

import org.junit.Before;
import org.junit.Test;

public class AtProtocolTest {
	private Protocol protocol;
	
	@Before
	public void setUp() throws Exception {
		protocol = new AtProtocol();
	}

	@Test
	public void testParseText() {
		Message message = protocol.parseText("@[result:\"hej med dig\"]");
		assertTrue(message.hasKey("result"));
		assertEquals("hej med dig", message.getString("result"));
	}

	@Test
	public void testToText() {
		Message message = protocol.parseText("@[result:\"hej med dig\"]");
		assertEquals("@[result:\"hej med dig\"]", protocol.toText(message));
	}

	@Test
	public void testNewMessage_1() {
		Message builder = protocol.newMessage();
		builder.put("result", "hej med dig");
		builder.put("isAdmin", Boolean.TRUE);
//		Message message = builder.toMessage();
		assertNotNull(builder);
	}
	
	@Test
	public void testNewMessage_2() {
		Message builder = protocol.newMessage();
		builder.put("result", "hej med dig");
		builder.put("isAdmin", Boolean.TRUE);
		Message message = builder; //.toMessage();
		Message messageCreatedByHand = protocol.parseText("@[result:\"hej med dig\"]");
		assertNotNull(builder);
		assertNotNull(message);
//		System.out.println(messageCreatedByHand.getString("result"));
//		System.out.println(message.getString("result"));
		assertEquals(messageCreatedByHand.getString("result"), message.getString("result"));
	}

	@Test
	public void testExtendMessage() {
		Message message = protocol.parseText("@[result:\"hej med dig\"]");
		assertNotNull(message);
		Message builder = protocol.extendMessage(message);
		builder.put("isAdmin", Boolean.TRUE);
//		message = builder.toMessage();
		assertTrue(builder.hasKey("result") && builder.hasKey("isAdmin"));
	}
	
	@Test
	public void testMessage() {
		Message message = protocol.newMessage();
		message.put("key", "value");
		String messageText = protocol.toText(message);
		assertEquals("@[key:value]", messageText);
	}
}